# deuley(1)

## NAME
Deuley -- design, develop, and market complex technology products

## SYNOPSIS
deuley [--help] [--ideate <topic?>] [--manage <product|project|program>] [--design <experience> <fidelity>] [--develop <prototype>] [--predict <future_state> <funny=True|False>] [--communicate <idea>] [--play <fps>]

## DESCRIPTION

The `deuley` utility is a flexibile tool the general development of software projects. It selects the appropriate subroutine given the current context, completes the task at hand, and returns the output in a timely manner. Included libraries offer a robust suite for the development and growth of software, ideation and commuication of concepts, and other general time wasting.

### The options are as follows:

**--help** Displays this list of options.

**--ideate <topic?>** Output a roughly formed concept. Given a topic, the utility will attempt to stay within the bounds of the target topic space (but often does not).

**--manage <product|project|program>** Invoke the appropriate subroutine based on the type of object passed. It accepts products, projects, and programs, and ensures on-time delivery and user satisfaction.

**--design <experience> <fidelity>** Return artifacts for the given `experience` at the requested `fidelity`. Capable of outputting at `low`, `mid`, and `high` fidelities, but note that higher fidelities take exponentially longer to return output.

**--develop <prototype>** Generate basic software protypes for the given concept. WARNING: DO NOT USE IN PRODUCTION.

**--predict <future_state> <funny?=True|False>** A port of the BSD utility `fortune`, but the database has been replaced with nerdy cyberpunk nonsense. Default `funny` value is `True`.

**--communicate <idea>** Return a highly verbose overview of the topic in question, often including diagrams and slidedecks.

**--play <game?>** Launches a video game (typically something twitchy). If no option is given, default target application is Counter Strike: Global Offensive.

**--jail** Restrict execution to current permissions space.

**--nice <value>**  Implements `nice(1)` to set the CPU priority of execution. If not specified, default niceness is 0.

## CONFIGURATION

Configuration of the `deuley` utility is deterministic. Store arbitrary plaintext in `/usr/bin/deuley/config.txt`, and at runtime, the utility parses the requested state, develops appropriate configuration values to achieve the desired outcome, and executes. NOTE: configuration *can* be altered during runtime, but the utility will throw numerous warnings and output tremendous volumes of logs.

## PERMISSIONS MODEL

`deuley` executes with the permissions of the user making the call, but in the event of encountering a permissions error, will escalate continuously until the call succeeeds, unless explicitly limited with the `--jail` flag.

## MULTITHREADING

By using the `--nice` flag, you can set the priority level of the exeuction. If no value is set, all jobs run at the same priority level. Default values for niceness can be set per-flag in `/usr/bin/deuley/config.txt`.

## SEE ALSO

explore(1), explain(1), rant(1), present(1), engage(1), code(1), illustrate(1), talk(1), goof(6), research(1), forecast(1), fortune(6)

- Douglas Adams, "The Hitchhikers Guide to the Galaxy", Pan Books, 1979
- Robert Bringhurst, "The Elements of Typographic Style", Hartley & Marks Publishers, 1992
- Hermann Hesse, "Siddhartha", New Directions, 1922
- The White Stripes, "Elephant", Third Man Records, 2003
- David Bowie, "The Man Who Sold the World", Mercury Records, 1970
- Disclosure, "Caracal", PMR, 2015
- Wes Anderson, "The Royal Tenenbaums", Touchstone Pictures, 2001
- "The Johnny Worricker Trilogy", BBC Films, 2011
- Tony Gilroy, "Michael Clayton", Warner Bros., 2007
- Assorted Participants, "The Carnival of Mirrors", Burning Man, Black Rock City, NV, 2015

